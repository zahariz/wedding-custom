import { createRouter, createWebHistory, createWebHashHistory } from 'vue-router';
import Home from '@/Template/Index.vue'
import Guests from '@/view/Guests/Index.vue'
import NotFound from '@/view/NotFound/Index.vue'

const routes = [
    {
        path: '/',
        name: 'home.index',
        component: Home,
    },
    {
        path: '/tamu',
        name: 'guests.index',
        component: Guests,
    },
    {
        path: '/:catchAll(.*)',
        component: NotFound,
    },
];

const router = createRouter({
    history: createWebHistory(),
    routes
})


export default router