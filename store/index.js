// store/index.js
import { createStore } from 'vuex';
import axios from 'axios';


export default createStore({
  state: {
    guests: [],
    tamus: []
  },
  mutations: {
    setGuests(state, guests) {
      state.guests = guests;
    },
    setTamus(state, tamus){
        state.tamus = tamus;
    },
    addGuest(state, guest) {
        console.log(guest);
    },
    addTamus(state, tamu) {
        console.log(tamu);
    }
  },
  actions: {
    async fetchGuests({ commit }) {
      const response = await axios.get('https://api-wedding-khaki.vercel.app/api/v1/guests');
      commit('setGuests', response.data);
    },
    async addGuest({ commit }, guest) {
        try {
            const response = await axios.post('https://api-wedding-khaki.vercel.app/api/v1/guests', guest);
            console.log(response);
            // Pastikan response yang diterima adalah objek tamu yang baru dibuat
            commit('addGuest', response.data.data);
          } catch (error) {
            console.error('Error adding guest:', error);
            throw error; // Melempar kembali error agar dapat ditangani di komponen Vue
          }
    },
    async fetchTamus({ commit }) {
        const response = await axios.get('https://api-wedding-khaki.vercel.app/api/v1/tamu');
        commit('setTamus', response.data);
      },
      async addTamus({ commit }, tamu) {
          try {
              const response = await axios.post('https://api-wedding-khaki.vercel.app/api/v1/tamu', tamu);
              console.log(response);
              // Pastikan response yang diterima adalah objek tamu yang baru dibuat
              commit('addTamus', response.data.data);
            } catch (error) {
              console.error('Error adding guest:', error);
              throw error; // Melempar kembali error agar dapat ditangani di komponen Vue
            }
      }
  },
  getters: {
    getGuests(state) {
      return state.guests;
    },
    getTamus(state) {
        return state.tamus;
    },
  }
});
